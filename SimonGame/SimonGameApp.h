#pragma once

#include "Application.h"
#include "Renderer2D.h"
#include "DoubleLinkedList.h"

class SimonGameApp : public aie::Application {
public:

	enum COLOURS
	{
		BLUE = 1,
		RED,
		YELLOW,
		GREEN
	};

	SimonGameApp();
	virtual ~SimonGameApp();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

private:

	void appendToPattern();

	aie::Renderer2D*	m_2dRenderer;
	aie::Font*			m_font;
	aie::Texture*		m_circle_Red;
	aie::Texture*		m_circle_Green;
	aie::Texture*		m_circle_Yellow;
	aie::Texture*		m_circle_Blue;
	aie::Texture*		m_Title;


	unsigned int		m_Count;
	unsigned int		m_Level;
	unsigned int		lastInput;
	float				m_Timer;

	float				m_TimerDelay;
	float				m_FastestTurn;
	float				m_TimerSpeedUp;
	float				pauseLength = 0.1f;
	float				m_UiRotation;
	float				m_EndOfTurnTimer = 0.35f;

	bool				pause = false;
	bool				true_Input = false;
	bool				gameOver = false;
	bool				gamePlaying = false;
	bool				failed = false;
	bool				isTurnEnded = false;

	DoubleLinkedList<int> m_pattern;
	DoubleLinkedList<int>::Iterator		m_patternIterator;
	DoubleLinkedList<int>::Iterator		m_playerIterator;
};