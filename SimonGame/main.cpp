#define _CRTDBG_MAP_ALLOC
#include "SimonGameApp.h"
#include <stdlib.h>
#include <crtdbg.h>

int main() {
	
	// allocation
	auto app = new SimonGameApp();

	// initialise and loop
	app->run("AIE", 1280, 720, false); // app starts here, calling appropriate draw and other startup functions under run

	// deallocation
	delete app;

	_CrtDumpMemoryLeaks();
	return 0;
}