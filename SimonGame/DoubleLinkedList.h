#pragma once

template <typename T>
class DoubleLinkedList
{
private:
	class Node
	{
	public:
		Node(T data, Node* next = nullptr, Node* previous = nullptr)
		{
			this->data = data;
			this->next = next;
			this->previous = previous;
		}

		T data;
		Node* next;
		Node* previous;
	};

public:
	class Iterator // Iterator is used to step through the list.  It can be used outside the class.
	{
	public:
		Iterator(Node* node = nullptr) // Default constructor
		{
			this->node = node;
		}

		~Iterator()
		{

		}

		bool isValid() const
		{
			return (node != nullptr);
		}

		T& getData() const
		{
			if (node == nullptr)
			{
				throw "Invalid iterator";
			}
			return node->data; // char at memory location
		}

		// Move to next node in the list.
		bool moveNext()
		{
			if (node != nullptr)
			{
				node = node->next;
			}
			
			return isValid();
		}

		// Move to the previous node in the list
		bool movePrevious()
		{
			if (node != nullptr)
			{
				node = node->next;
			}

			return isValid();
		}

	private:
		friend class DoubleLinkedList;
		Node* node;
	};
	
public:
	DoubleLinkedList()
	{
		head = tail = nullptr;
	}

	~DoubleLinkedList()
	{
		clear();
	};

	// Check if the list is empty
	bool empty() const
	{
		return (head == nullptr) || (tail == nullptr);
	}


	// delet all nodes.
	void clear()
	{
		Node* node = head;
		Node* nodeToDelete;
		// Loop through all nodes starting from the head until it hits a nullptr marking the end of the list. 
		//Get a temp copy of the node to delete and move node to the next one
		while (node != nullptr)
		{
			nodeToDelete = node;
			node = node->next;
			delete nodeToDelete;
		}
		head = tail = nullptr;
	}

	// Pushes data onto the front of the list.
	void pushFront(const T& data)
	{
		// Create a new node with this input data.  Makes it's next point to the current head
		Node* newNode = new Node(data, head);
		
		if (head != nullptr)
		{
			head->previous = newNode;
		}
		head = newNode;

		if (tail == nullptr)
		{
			tail = newNode;
		}
	}

	// Pushes data onto the end of the list.
	void pushBack(const T& data)
	{
		// Create a new node with this input data.  Makes it's next point to the current tail
		Node* newNode = new Node(data, nullptr, tail);

		if (tail != nullptr)
		{
			tail->next = newNode;
		}
		tail = newNode;

		if (head == nullptr)
		{
			head = newNode;
		}
	}

	// Pops data off the front of the list.  Returns data on success.  Throws exception if bad.
	T popFront()
	{
		if (head == nullptr)
		{
			throw "List is empty";
		}

		// Get a reference to the node with the data
		Node* n = head;

		// Move the head/tail
		if (head == tail)
		{
			head = tail = nullptr;
		}
		else
		{
			head = head->next;
			head->previous = nullptr;
		}

		// Get a temp copy of the node data
		T data = n->data;

		// delet node data
		delete n;
		return data;
	}

	T popBack()	// Pops data off the back of the list.  Returns data on success.  Throws exception if bad.
	{
		if (tail == nullptr)
		{
			throw "List is empty";
		}

		// Get a reference to the node with the data
		Node* n = tail;

		// Move the head/tail
		if (head == tail)
		{
			head = tail = nullptr;
		}
		else
		{
			tail = tail->previous;
			tail->next = nullptr;
		}

		// Get a temp copy of the node data
		T data = n->data;
		   
		// delet node data
		delete n;
		return data;
	}

	T& first() const // Return the first element in the list
	{
		if (head == nullptr)
		{
			throw "List empty";
		}

		return head->data;
	}


	T& last() const // Return the last data element in the list
	{
		if (tail == nullptr)
		{
			throw "List empty";
		}

		return tail->data;
	}
	
	Iterator begin() const // returns iterator to head
	{
		return Iterator(head);
	}

	Iterator end() const // returns iterator to tail
	{
		return Iterator(tail);
	}

	void insert(Iterator iteratorLocation, T value) // inserts value to iterator location
	{
		Node* insertNode = iteratorLocation.node;

		if (insertNode != nullptr) // checks if pushback wont work
		{
			Node* newNode = new Node(value, insertNode->next, insertNode); // creates new node

			if (insertNode->next != nullptr) 
			{
				insertNode->next->previous = newNode;
			}

			insertNode->next = newNode;
		}
		else
		{
			pushBack(value);
		}
	}

	void erase(Iterator iterator) // erases a single node
	{
		Node* eraseNode = iterator.node;

		if (eraseNode != nullptr)
		{
			if (eraseNode->next == nullptr)
			{
				popBack();
			}
			else if (eraseNode->previous == nullptr)
			{
				popFront();
			}
			else
			{
				eraseNode->next->previous = eraseNode->previous;
				eraseNode->previous->next = eraseNode->next;
				eraseNode = nullptr;
			}
		}
	}

	void remove(T value)
	{
		Node* lookAt = head;

		while (lookAt != nullptr)
		{
			if (lookAt->data == value)
			{
				if (lookAt->previous == nullptr)
				{
					lookAt = lookAt->next;
					popFront();
				}
				else if (lookAt->next == nullptr)
				{
					lookAt = lookAt->next;
					popBack();
				}
				else
				{
					lookAt->next->previous = lookAt->previous;
					lookAt->previous->next = lookAt->next;
					lookAt = lookAt->next;
					lookAt->previous = nullptr;
				}
			}
			else
			{
				lookAt = lookAt->next;
			}
		}
	}


	unsigned int count() const // Return the number of items in the list
	{
		unsigned int counter = 0;
		Node* node = head;
		while (node != nullptr)
		{
			counter++;
			node = node->next;
		}
		return counter;
	}

private:
	Node* head;
	Node* tail;
};