#include "SimonGameApp.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include <time.h>
#include <sstream>

SimonGameApp::SimonGameApp() {

}

SimonGameApp::~SimonGameApp() {

}

bool SimonGameApp::startup() {
	m_2dRenderer = new aie::Renderer2D();

	srand(time(nullptr));

	// TODO: remember to change this when redistributing a build!
	// the following path would be used instead: "./font/consolas.ttf"
	m_font = new aie::Font("../bin/font/roboto/Roboto-Regular.ttf", 32);

	m_circle_Red = new aie::Texture("../textures/red.png");
	m_circle_Green = new aie::Texture("../textures/green.png");
	m_circle_Yellow = new aie::Texture("../textures/yellow.png");
	m_circle_Blue = new aie::Texture("../textures/blue.png");
	m_Title = new aie::Texture("../textures/simonlogo.png");


	m_Timer = 0, m_Level = 1, m_TimerDelay = 1, m_FastestTurn = 0.4f, m_TimerSpeedUp = 0.1f, m_UiRotation = -.74f, m_Count = 0;

	//for (unsigned int i = 0; i < 5; i++)
		appendToPattern();

	m_patternIterator = m_pattern.begin();
	m_playerIterator = m_pattern.begin();

	return true;
}

void SimonGameApp::shutdown() {

	delete m_font;
	delete m_2dRenderer;
	delete m_circle_Red;
	delete m_circle_Green;
	delete m_circle_Yellow;
	delete m_circle_Blue;
	delete m_Title;
}

void SimonGameApp::update(float deltaTime) {

	// input example
	aie::Input* input = aie::Input::getInstance();

	// exit the application
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();


	m_Timer += deltaTime;

	if (m_Timer > m_TimerDelay && gamePlaying) // pause between simon flashes
	{
		m_Timer = 0;
		pause = true;
		m_patternIterator.moveNext();
	}

	if (!gamePlaying) // press enter to start logic
	{
		if (input->wasKeyPressed(aie::INPUT_KEY_ENTER))
		{
			gamePlaying = true;
			gameOver = false;
			m_pattern.clear();
			appendToPattern();
			m_patternIterator = m_pattern.begin();
			m_playerIterator = m_pattern.begin();
			m_Timer = 0;
			m_Level = 1;
			
		}
	}

	// Is the player 
	if (!m_patternIterator.isValid() && !gameOver && gamePlaying)
	{
		if (m_playerIterator.isValid())
		{
			if (input->wasKeyPressed(aie::INPUT_KEY_UP)) // player red
			{
				m_playerIterator.getData() == RED ? m_playerIterator.moveNext() : gameOver = true;
				m_Timer = 0;
			}

			if (input->wasKeyPressed(aie::INPUT_KEY_LEFT)) // player green
			{
				m_playerIterator.getData() == GREEN ? m_playerIterator.moveNext() : gameOver = true;
				m_Timer = 0;
			}

			if (input->wasKeyPressed(aie::INPUT_KEY_DOWN)) // player yellow
			{
				m_playerIterator.getData() == YELLOW ? m_playerIterator.moveNext() : gameOver = true;
				m_Timer = 0;
			}

			if (input->wasKeyPressed(aie::INPUT_KEY_RIGHT)) // player blue
			{
				m_playerIterator.getData() == BLUE ? m_playerIterator.moveNext() : gameOver = true;
				m_Timer = 0;
			}
		}

	}
	
	if (!m_playerIterator.isValid() && m_Timer > m_EndOfTurnTimer) // if player matched all elements in pattern list
	{

		m_Count = 0;
		m_playerIterator = m_pattern.begin();
		m_patternIterator = m_pattern.begin();
		appendToPattern();
		m_Level++;
		m_Timer = 0;
		isTurnEnded = false;
		
		if (m_TimerDelay >= m_FastestTurn) // speed up if not max speed
		{
			m_TimerDelay -= m_TimerSpeedUp;
		}

	// increment level difficulty by adding to the end of the list
	}
}

void SimonGameApp::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// begin drawing sprites
	m_2dRenderer->begin();
	aie::Input* input = aie::Input::getInstance();
	// Draw all button slightly dimmed


	if (!gamePlaying) // game only runs if gamePlying
	{
		m_2dRenderer->drawText(m_font, "Press Enter to Start", 490, 340);
	}
	else // draw greyed out
	{
	m_2dRenderer->setRenderColour(.5, .5, .5);
	m_2dRenderer->drawSprite(m_circle_Blue, 620, 314, 200, 200, m_UiRotation); // x, y
	
	m_2dRenderer->drawSprite(m_circle_Red, 480, 445, 200, 200, m_UiRotation);

	m_2dRenderer->drawSprite(m_circle_Yellow, 490, 170, 200, 200, m_UiRotation);

	m_2dRenderer->drawSprite(m_circle_Green, 350, 300, 200, 200, m_UiRotation);
	m_2dRenderer->setRenderColour(1, 1, 1);
	m_2dRenderer->drawSprite(m_Title, 484, 310);

	// Level text
	std::stringstream s;
	s << "Level ";
	s << m_Level;

	std::string uiText = s.str();
	m_2dRenderer->drawText(m_font, uiText.c_str(), 770, 550);
	
	}


	if (gameOver) // if lost
	{
		m_2dRenderer->setRenderColour(1, 1, 1);
		// Level text
		std::stringstream s;
		s << "Incorrect, you have lost at level ";
		s << m_Level;

		std::string uiText = s.str();
		m_2dRenderer->drawText(m_font, uiText.c_str(), 300, 550);
		gamePlaying = false;
		m_TimerDelay = 1;
	}

	if (m_patternIterator.isValid() && !gameOver && gamePlaying) // checks if pattern iterator is still looking at pattern || "Simon's Turn"
	{
		m_2dRenderer->setRenderColour(1, 1, 1);
		m_2dRenderer->drawText(m_font, "Simon's Turn", 380, 550); 
		
		if (m_Timer >= pauseLength && pause == true) // pauses simon starting turn to display players final input
		{
			pause = false;
		}
		else if (pause == false)
		{
			switch (m_patternIterator.getData())
			{
			case BLUE:
				m_2dRenderer->setRenderColour(1, 1, 1);
				m_2dRenderer->drawSprite(m_circle_Blue, 620, 314, 200, 200, m_UiRotation);
				break;
			case RED:
				m_2dRenderer->setRenderColour(1, 1, 1);
				m_2dRenderer->drawSprite(m_circle_Red, 480, 445, 200, 200, m_UiRotation);
				break;
			case YELLOW:
				m_2dRenderer->setRenderColour(1, 1, 1);
				m_2dRenderer->drawSprite(m_circle_Yellow, 490, 170, 200, 200, m_UiRotation);
				break;
			case GREEN:
				m_2dRenderer->setRenderColour(1, 1, 1);
				m_2dRenderer->drawSprite(m_circle_Green, 350, 300, 200, 200, m_UiRotation);
				break;
			default:
				break;
			}
		}
		
	}
	else if (!gameOver && gamePlaying && isTurnEnded == false) // displaying player inputs unless turn is ended
	{
		m_2dRenderer->setRenderColour(1, 1, 1);
		m_2dRenderer->drawText(m_font, "Player's Turn", 380, 550);

		if (input->isKeyDown(aie::INPUT_KEY_UP)) // player red
		{ 		
			m_2dRenderer->drawSprite(m_circle_Red, 480, 445, 200, 200, m_UiRotation);
		}

		if (input->isKeyDown(aie::INPUT_KEY_LEFT)) // player green
		{		
			m_2dRenderer->drawSprite(m_circle_Green, 350, 300, 200, 200, m_UiRotation);
		}

		if (input->isKeyDown(aie::INPUT_KEY_DOWN)) // player yellow
		{		
			m_2dRenderer->drawSprite(m_circle_Yellow, 490, 170, 200, 200, m_UiRotation);
		}

		if (input->isKeyDown(aie::INPUT_KEY_RIGHT)) // player blue
		{		
			m_2dRenderer->drawSprite(m_circle_Blue, 620, 314, 200, 200, m_UiRotation);
		}

		if (!m_playerIterator.isValid()) {
			isTurnEnded = true;

		}
	}

	
	m_2dRenderer->setRenderColour(1, 1, 1);
	m_2dRenderer->drawText(m_font, "Press ESC to quit", 0, 0);
	
	// done drawing sprites
	m_2dRenderer->end();
}


void SimonGameApp::appendToPattern() // appends new data to pattern
{
	m_pattern.pushBack(1 + (rand() % (4)));
}